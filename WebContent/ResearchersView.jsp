<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<%@ include file = "Header.jsp"%>
<title>Researchers</title>
</head>
<body>
<table>
<tr>
<td>  ${ri.id} </td>
<td>${ri.name}</td>
                <td>${ri.lastname}</td>
                <td><a href="${ri.scopusURL}">${ri.scopusURL}</a></td>
                <td>${ri.email}</td>

</tr>
</table>
<!-- 
<a href="UpdatePublicationsResearcherServlet?id=${pi.id}"> ${pi.id}</a>
- -->
<c:if test="${ri.id == user.id}">
<form action="CreatePublicationServlet" method="post">
        <input type="text" name="id" placeholder="User Id">
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="authors" placeholder="Authors">
        <input type="text" name="citeCount" placeholder="Cites">
        <input type="text" name="publicationName" placeholder="Publication Name">
        <input type="text" name="publicationDate" placeholder="Publication Date">
        <button type="submit">Create researcher</button>
</form>>
</c:if>
<table>
<tr>
<th>Publications</th>
</tr>
<c:forEach items="${publications}" var="pi">
        <tr>
        		<td> <a href="PublicationServlet?id=${pi.id}"> ${pi.id}</a></td>
				
        </tr>
</c:forEach>
</table>
</body>
</html>