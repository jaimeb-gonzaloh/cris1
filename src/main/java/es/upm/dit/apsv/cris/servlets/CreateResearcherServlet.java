package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import java.net.URLEncoder;

import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreateResearcherServlet
 */
@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	final String ADMIN = "root";
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
	
		
		 Researcher r = (Researcher) request.getSession().getAttribute("user");
		   if(( r!=null) && r.getId().equals("root")) {
			   Researcher ri=new Researcher();
				ri.setId( (String)request.getParameter("id"));
				ri.setName((String)request.getParameter("name"));
				ri.setLastname( (String)request.getParameter("lastname"));
				ri.setEmail( (String)request.getParameter("email"));
				ri.setPassword( (String)request.getParameter("password"));
				Client client = ClientBuilder.newClient(new ClientConfig());
				
				
				Response resp = client.register(JsonProcessingFeature.class)
						.target("http://localhost:8080/CRISSERVICE/rest/Researchers/" ).request()
						                .post(Entity.entity(ri, MediaType.APPLICATION_JSON), Response.class);
				
			
		
				response.sendRedirect(request.getContextPath() + "/AdminServlet");
			   
		   }
		   else {
			   getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		   }


		
	   

}
}


