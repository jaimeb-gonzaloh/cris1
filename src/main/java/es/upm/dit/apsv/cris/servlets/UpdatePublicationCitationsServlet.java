

package es.upm.dit.apsv.cris.servlets;
import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;




@WebServlet("/UpdateCitationsPublicationsServlet")
public class UpdatePublicationCitationsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Publication pi =  (Publication) request.getAttribute("pi");
		Client client = ClientBuilder.newClient(new ClientConfig());
		client.target("http://localhost:8080/CRISSERVICE/rest/Researchers/" 
		         + pi.getId()+"/UpdateCiteNumber")
		        .request().accept(MediaType.APPLICATION_JSON).get(Researcher.class);  
		request.setAttribute ("pi", pi);
		
	    response.sendRedirect(request.getContextPath() + "/PublicationServlet" + "?id="
	        + pi.getId());

	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}