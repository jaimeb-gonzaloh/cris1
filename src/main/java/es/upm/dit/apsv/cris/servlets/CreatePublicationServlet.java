package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jsonp.JsonProcessingFeature;

import es.upm.dit.apsv.cris.model.Researcher;
import es.upm.dit.apsv.cris.model.Publication;
/**
 * Servlet implementation class CreatePublicationServlet
 */
@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	final String ADMIN = "root";
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
		
	
		
		 Researcher r = (Researcher) request.getSession().getAttribute("user");
		   
			   Publication pi=new Publication();
				pi.setId( (String)request.getParameter("id"));
				pi.setTitle((String)request.getParameter("title"));
				pi.setAuthors( (String)request.getParameter("authors"));
				pi.setPublicationName( (String)request.getParameter("publicationName"));
				pi.setPublicationDate((String)request.getParameter("PublicationDate"));
				
				Client client = ClientBuilder.newClient(new ClientConfig());
				Response resp = client.register(JsonProcessingFeature.class)
						.target("http://localhost:8080/CRISSERVICE/rest/Publication/" ).request()
						                .post(Entity.entity(pi, MediaType.APPLICATION_JSON), Response.class);
				
				Publication ppi = client.target("http://localhost:8080/CRISSERVICE/rest/Publications/" 
				         + pi.getId())
				        .request().accept(MediaType.APPLICATION_JSON).get(Publication.class);  
				request.setAttribute ("pi", ppi);
				getServletContext().getRequestDispatcher("/PublicationView.jsp").forward(request, response);
				
			
				

			   
		   
		   

}
}
